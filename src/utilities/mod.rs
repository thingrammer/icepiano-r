//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use num_traits::real::Real;

pub fn midi_to_dbfs(velocity: u8, express: u8) -> f64 {
    let velocity: f64 = velocity as f64;
    let express: f64 = express as f64;
    if velocity == 0.0 || express == 0.0 {
        -999.0
    } else {
        40.0 * ((velocity*express)/(127.0*127.0)).ln()
    }
}

pub fn dbfs_to_amp(dbfs: f64) -> f64 {
    (dbfs / 8.65618).exp()
}

pub fn find_closest<T: PartialOrd + Copy + Real>(input: T, list: &[T]) -> (usize, T) {
    let mut closest_gap: T = (input - list[0]).abs();
    let mut closest_index: usize = 0;
    let mut index: usize = 0;
    for &item in list {
        let new_gap: T = (input - item).abs();
        if new_gap < closest_gap {
            closest_gap = new_gap;
            closest_index = index;
        }
        index = index + 1;
    }
    (closest_index, list[closest_index])
}
