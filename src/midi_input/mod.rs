//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use std::io::{stdin, stdout, Write};
use std::process::exit;
use std::error::Error;
use std::sync::{Arc, Mutex};
use std::{thread, time::Duration};

use midir::{MidiInput, Ignore};

pub struct MidiRunner {
    midi_input: String,
}

impl MidiRunner {

    pub fn new() -> MidiRunner {
        MidiRunner {
            midi_input: String::new(),
        }
    }
    pub fn input_select (&mut self) -> Result<(), Box<dyn Error>> {
        let mut midi_in = MidiInput::new("midir test input")?;
        midi_in.ignore(Ignore::None);
        let in_ports = midi_in.ports();
        match in_ports.len() {
            0 => {
                println!("no input port found");
                exit(1);
            },
            1 => {
                println!("Choosing the only available input port: {}", midi_in.port_name(&in_ports[0]).unwrap());
                self.midi_input = String::from("0");
            },
            _ => {
                println!("\nAvailable input ports:");
                for (i, p) in in_ports.iter().enumerate() {
                    println!("{}: {}", i, midi_in.port_name(p).unwrap());
                }
                print!("Please select input port: ");
                stdout().flush()?;
                self.midi_input.clear();
                stdin().read_line(&mut self.midi_input)?;
            }
        };
        match in_ports.get(self.midi_input.trim().parse::<usize>()?).ok_or("invalid input port selected") {
            Ok(_) => (),
            Err(err) => {
                println!("{}", err);
                exit(1);
            }
        }
        
        Ok(())
    }

    pub fn run_reader (&mut self, midi_bridge_ct: Arc<Mutex<[u64; 4]>>) -> Result<(), Box<dyn Error>> {
        // let mut input = String::new();
        let mut midi_in = MidiInput::new("midir reading input")?;
        midi_in.ignore(Ignore::None);
        
        // Get an input port (read from console if multiple are available)
        let in_ports = midi_in.ports();
        let in_port = in_ports.get(self.midi_input.trim().parse::<usize>()?).ok_or("invalid input port selected")?;
        
        println!("\nOpening connection");
        let in_port_name = midi_in.port_name(in_port)?;

        // _conn_in needs to be a named parameter, because it needs to be kept alive until the end of the scope
        let _conn_in = midi_in.connect(in_port, "midir-read-input", move |stamp, message, midi_bridge_ct| {
            //println!("{}: {:?} (len = {})", stamp, message, message.len());
            let mut midi_modifier = midi_bridge_ct.lock().unwrap();
            *midi_modifier = [0, 0, 0, 0];
            midi_modifier[0] = stamp;
            let mut i = 0;
            while i < message.len() && message.len() <= 3 {
                midi_modifier[i+1] = message[i] as u64;
                i = i + 1;
            }
        }, midi_bridge_ct)?;

        println!("Connection open, reading input from '{}' ...", in_port_name);
        
        // input.clear();
        // stdin().read_line(&mut input)?; // wait for next enter key press

        // println!("Closing connection");
        loop {
            thread::sleep(Duration::from_secs(3600));
        }
        // Ok(())
    }
}
