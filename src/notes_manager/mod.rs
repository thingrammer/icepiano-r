//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use sdl2::mixer::Chunk;
use std::path::Path;
use sdl2::mixer::{Channel, Fading};
use crate::utilities::{midi_to_dbfs, dbfs_to_amp, find_closest};
use std::sync::{Arc, Mutex};

// #[derive(Debug)]
pub struct Sound {
    pub source: String,
    pub sound_chunk: Chunk,
    pub note: usize,
    pub dbfs: f64,
}

impl Sound {
    pub fn new (path_str: &str) -> Sound {
        let file_path = Path::new(path_str);
        let sound_chunk = Chunk::from_file(file_path).unwrap();
        let find_str = "processed-samples/";
        let findname_first = path_str.find(find_str).unwrap() + find_str.len();
        let findname_last = path_str.len();
        let findname = &path_str[findname_first..findname_last];
        let note: usize = findname[0..3].parse().unwrap();
        let ext_pos = findname.find(".wav").unwrap();
        let dbfs: f64 = findname[3..ext_pos].parse().unwrap();

        Sound {
            sound_chunk: sound_chunk,
            source: String::from(path_str),
            note: note,
            dbfs: dbfs,
        }
    }
}

pub struct Note {
    pub note: usize,
    pub sounds: [Option<Sound>; 16],
    pub dbfs_list: [f64; 16],
}

impl Note {
    pub fn new (note: usize) -> Note {
        const INIT: Option<Sound> = None;
        Note {
            note: note,
            sounds: [INIT; 16],
            dbfs_list: [0.0; 16],
        }
    }

    pub fn add_sound (&mut self, sound: Sound) {
        let mut i = 0;
        while i < 16 {
            if let None = self.sounds[i] {
                self.sounds[i] = Some(sound);
                self.dbfs_list[i] = self.sounds[i].as_ref().unwrap().dbfs;
                break;
            }
            i = i + 1;
        }
    }
}

pub struct PlayNotes {
    pub notes: [Option<Note>; 128],
    pub channel_loop: i32,
}

impl PlayNotes {
    pub fn new() -> PlayNotes {
        const INIT_NOTE: Option<Note> = None;
        let mut notes: [Option<Note>; 128] = [INIT_NOTE; 128];
        let mut i = 0;
        while i < 128 {
            notes[i] = Some(Note::new(i));
            i = i + 1;
        }
        PlayNotes {
            notes: notes,
            channel_loop: 0,
        }
    }

    pub fn play(&mut self, note: usize, velocity: u8, express: u8, channel_notes: &mut [usize; crate::CHANNEL_SIZE]) {
        channel_notes[self.channel_loop as usize] = note;
        let dbfs = midi_to_dbfs(velocity, express);
        let amp = dbfs_to_amp(dbfs);
        let set_volume: i32 = (amp * 128.0) as i32;
        let dbfs_list = &self.notes[note].as_ref().unwrap().dbfs_list;
        let (closest_index, _cloest_data) = find_closest(dbfs, dbfs_list);
        let chunk = &self.notes[note].as_ref().unwrap().sounds[closest_index].as_ref().unwrap().sound_chunk;
        let mut i: i32 = 0;
        let mut fading = true;
        while fading {
            let check_fading = Channel(self.channel_loop + i).get_fading();
            match check_fading {
                Fading::NoFading => {
                    fading = false;
                    if Channel(self.channel_loop + i).is_playing() {
                        Channel(self.channel_loop + i).halt();
                    }
                    Channel(self.channel_loop + i).set_volume(set_volume);
                    match Channel(self.channel_loop + i).play(&chunk, 0) {
                        Err(e) =>println!("{:?}", e),
                        _ => ()
                    }
                },
                _ => {
                    fading = true;
                    i = i + 1;
                }
            }
        }
        if self.channel_loop + i < crate::CHANNEL_SIZE as i32 - 2 {
            self.channel_loop = self.channel_loop + i + 1;
        } else {
            self.channel_loop = 0;
        }
    }
}

pub fn stop_notes(channel_notes_bridge:
                  &Arc<Mutex<[usize; crate::CHANNEL_SIZE]>>,
                  keys_bridge: &Arc<Mutex<[u8; 128]>>,
                  mid_pedal_bridge: &Arc<Mutex<[bool; 128]>>,
                  sustain_pedal_bridge: &Arc<Mutex<u8>>,
                  cur_channel_loop_rcv: &Arc<Mutex<i32>>){
    let mut i = crate::CHANNEL_SIZE - 1;
    loop {
        let channel_notes = *channel_notes_bridge.lock().unwrap();
        let cur_channel_loop = *cur_channel_loop_rcv.lock().unwrap();
        if  cur_channel_loop != i as i32 {
            let mid_pedal_hold = *mid_pedal_bridge.lock().unwrap();
            if mid_pedal_hold[channel_notes[i]] == false {
                let sustain_pedal = *sustain_pedal_bridge.lock().unwrap();
                if sustain_pedal == 0 {
                    let check_fading = Channel(i as i32).get_fading();
                    match check_fading {
                        Fading::NoFading => {
                            let keys_received = *keys_bridge
                                .lock().unwrap();
                            if keys_received[channel_notes[i]] == 0 {
                                Channel(i as i32).fade_out(260);
                            }
                        },
                        _ => {},
                    }
                }
            }
        }
        if i <= 0 {
            i = crate::CHANNEL_SIZE - 1;
        } else {
            i = i - 1;
        }
    }
}
