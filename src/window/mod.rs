//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use sdl2;
use sdl2::keyboard::Keycode;
use sdl2::event::Event;
use std::sync::{Arc, Mutex};
use sdl2::image::{self, LoadTexture, InitFlag};
use sdl2::rect::Rect;
use sdl2::render::{WindowCanvas, Texture};
use std::path::Path;
use std::fs;

pub fn image_render(canvas: &mut WindowCanvas,
                    keys_bridge: &Arc<Mutex<[u8; 128]>>,
                    keyboard_base: &Texture,
                    note_images: &mut [Option<Texture>; 128],
                    pedal_images: &mut (Texture, Texture, Texture),
                    pedals_bridge: &Arc<Mutex<(u8, u8, u8)>>) {
    let keys_get = *keys_bridge.lock().unwrap();
    let pedals_get = *pedals_bridge.lock().unwrap();
    let (sustain, express, mid) = pedals_get;
    let sustain_alpha = (sustain as f32 * 1.6) as u8;
    let express_alpha = ((127 - express) as f32 * 1.6) as u8;
    let mid_alpha = (mid as f32 * 1.6) as u8;
    let (sustain_image, express_image, mid_image) = pedal_images;
    sustain_image.set_alpha_mod(sustain_alpha);
    express_image.set_alpha_mod(express_alpha);
    mid_image.set_alpha_mod(mid_alpha);
    canvas.clear();
    match canvas.copy(keyboard_base, None, None) {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err)
    }
    match canvas.copy(sustain_image, None, None) {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err)
    }
    match canvas.copy(express_image, None, None) {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err)
    }
    match canvas.copy(mid_image, None, None) {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err)
    }
    let mut wc = 0;
    while wc < 128 {
        let note_alpha = (keys_get[wc] as f32 * 1.6) as u8;
        let note_texture = note_images[wc].as_mut();
        if note_texture.is_some() {
            let note_texture: &mut Texture = note_texture.unwrap();
            note_texture.set_alpha_mod(note_alpha);
            match canvas.copy(note_texture, None, Rect::new(0, 0, 1920, 257)) {
                Ok(_) => (),
                Err(err) => println!("Error: {}", err)
            }
        }
        wc = wc + 1;
    }
    canvas.present();
}

pub fn display_window(exit_bridge_ct: Arc<Mutex<bool>>,
                      keys_bridge: Arc<Mutex<[u8; 128]>>,
                      pedals_bridge: Arc<Mutex<(u8, u8, u8)>>,
                      selected_channel_snd: Arc<Mutex<u8>>) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let _image_context = image::init(InitFlag::PNG).unwrap();
    let window = video_subsystem.window("IcePiano-R", 1920, 420)
        .build()
        .unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut canvas = window.into_canvas()
        .present_vsync()
        .build().unwrap();
    let texture_creator = canvas.texture_creator();
    let keyboard_base = texture_creator.load_texture("./data/images/keyboard-88.png").unwrap();
    const INIT_TEXTURE: Option<Texture> = None;
    let mut note_images: [Option<Texture>; 128] = [INIT_TEXTURE; 128];
    let note_key_paths = fs::read_dir("./data/images").unwrap();
        
    for path in note_key_paths {
        let path_string = path.unwrap().path().display().to_string();
        if path_string.find("key-").is_some()
            && path_string.find(".png").is_some() {
                let note_start = path_string.find("key-").unwrap() + "key-".len();
                let note_end = path_string.find(".png").unwrap();
                let note: usize = path_string[note_start..note_end]
                    .parse()
                    .unwrap();
                let path_str = path_string.as_str();
                let note_key_path = Path::new(path_str);
                let note_key_texture = texture_creator.load_texture(note_key_path).unwrap();
                note_images[note] = Some(note_key_texture);
            }
    }
    let mut pedal_images: (Texture, Texture, Texture) = (
        texture_creator.load_texture("./data/images/sus-pd.png").unwrap(),
        texture_creator.load_texture("./data/images/soft-pd.png").unwrap(),
        texture_creator.load_texture("./data/images/sos-pd.png").unwrap()
    );
    let mut channel_sel_mode = false;
    let mut ctrl_down = false;
    let mut input_text = String::new();
    'display_loop: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    let mut should_exit = exit_bridge_ct.lock().unwrap();
                    *should_exit = true;
                    break 'display_loop;
                },
                Event::KeyDown { keycode: Some(Keycode::LCtrl), .. } => {
                    ctrl_down = true;
                },
                Event::KeyUp { keycode: Some(Keycode::LCtrl), .. } => {
                    ctrl_down = false;
                },
                Event::KeyDown { keycode: Some(Keycode::P), .. } => {
                    if ctrl_down && channel_sel_mode == false {
                        channel_sel_mode = true;
                        println!("Channel selection mode: {}",
                                 channel_sel_mode);
                        input_text = String::new();
                    } else if ctrl_down {
                        channel_sel_mode = false;
                        println!("Channel selection mode: {}",
                                 channel_sel_mode);
                        input_text = String::new();
                    }
                },
                Event::TextInput {text, ..} => {
                    if channel_sel_mode {
                        input_text = input_text + text.as_str();
                        println!("Channel number input: {}", input_text);
                    }
                },
                Event::KeyDown {keycode: Some(Keycode::Return), ..} => {
                    if channel_sel_mode {
                        channel_sel_mode = false;
                        let mut channel_input = selected_channel_snd
                            .lock().unwrap();
                        *channel_input = input_text.parse().unwrap();
                        println!("Your new midi channel: {}", channel_input);
                        input_text = String::new();
                    }
                },
                _ => {}
            }
        }
        image_render(&mut canvas,
                     &keys_bridge,
                     &keyboard_base,
                     &mut note_images,
                     &mut pedal_images,
                     &pedals_bridge);
    }
}
