# IcePiano-R

## Introduction

This program is the re-write of the IcePiano in Rust programming
language. The goal of this rewritting is to get the same usability of
the old IcePiano. It should be able to playback for midi keyboards;
reproduce some major features of real acoustic piano; have a graphical
display on the screen so the player can see which keys and pedals have
been applied.

This program is now very usable. Just plug your midi keyboard in, and
follow the Installation section of this README, then you are good to
go!

## Installation

Dependencies:

SDL2 >= 2.0.8

SDL2_mixer >= 2.0.4

SDL2_image >= 2.0.5

Install Rust toolchain first (https://rustup.rs/), then:

For the first time build:

    git clone https://gitlab.com/iceguye/icepiano-r
    cd icepiano-r
    cargo build --release

For updating to the latest version:

    cd icepiano-r
    git pull
    cargo build --release

## Usage

Plug your USB midi keyboard to your computer.

Then run: 

    cargo run --release

To change the midi channel, we use some sort of Emacs combination keys
style. This must be done while the window of IcePiano-R is active;
this cannot be done in your terminal, or the program may quite.

   ctrl+p MIDI_CHANNEL_NUMBER Return

## Authors

- IceGuye

    -- Main developer

- Alexander Holm

    -- Who provided the samples freely, email: axeldenstore (at) gmail
       (dot) com

- Jiao Yidi

    -- Professional pianist, who provided research data for this
       project, especially for the stoppers' behaviors. Website:
       jiaoyidi.art

- Hao

    -- Physicist, provided huge supports and contributions to the
       program of perfect harmonic series.

## Copyright

Copyright © 2021-2022 IceGuye

Unless otherwise noted, all files in this project, including but NOT
limited to source codes and art contents, are free software: you can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, version 3
of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.